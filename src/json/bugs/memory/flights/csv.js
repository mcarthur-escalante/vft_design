const fs = require('fs');

const flightInfo =
  require('./flightsBatch_1')
    .concat(require('./flightsBatch_2'))
    .concat(require('./flightsBatch_3'))
    .concat(require('./flightsBatch_4'))
    .concat(require('./flightsBatch_5'))
;

const header = 'FLIGHTS_BEFORE\tFLIGHTS_AFTER\tUSED_MEM_BEFORE\tUSED_MEM_AFTER\n';
let contents = '';

flightInfo.forEach(info => {
  contents += `${info.flightsBefore}\t${info.flightsAfter}\t${info.usedMemBefore}\t${info.usedMemAfter}\n`
})

fs.writeFileSync('./flights.csv', header + contents, 'utf8');