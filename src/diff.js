const fs = require('fs');
const diff = require('deep-diff');
const Flatted = require('flatted');

const DIR = 'C:\\PC\\VFT_Design\\src\\json\\bugs\\debugme3\\settings\\mapbox_map\\';

const COMPARISONS = [
  {
    leftSide: 'arturo_settings_dtw_surface.json',
    rightSide: 'debugme_3_settings_dtw_surface.json',
    diff: 'diff_settings_dtw_surface.json'
  },
  {
    leftSide: 'arturo_settings_dtw_weather.json',
    rightSide: 'debugme_3_settings_dtw_weather.json',
    diff: 'diff_settings_dtw_weather.json'
  },
]

COMPARISONS.forEach(comparison => {
  console.log(`Reading left object (${comparison.leftSide})...`)
  const leftProps = Flatted.parse(fs.readFileSync(`${DIR}${comparison.leftSide}`, 'utf8'))

  console.log(`Reading right object (${comparison.rightSide})...`)
  const rightProps = Flatted.parse(fs.readFileSync(`${DIR}${comparison.rightSide}`, 'utf8'))

  console.log('Calculating differences...')
  const differences = diff(leftProps, rightProps);

  const newProperties = differences.filter(difference => difference.kind === 'N')
  const deletedProperties = differences.filter(difference => difference.kind === 'D')
  const changedProperties = differences.filter(difference => difference.kind === 'E')
  const arrayChanges = differences.filter(difference => difference.kind === 'A')

  console.log(`\nTotal differences: ${differences.length}`)
  console.log(`New properties (N): ${newProperties.length}`)
  console.log(`Deleted properties (D): ${deletedProperties.length}`)
  console.log(`Changed properties (E): ${changedProperties.length}`)
  console.log(`Array changes (A): ${arrayChanges.length}\n`)

  console.log('Saving differences...')
  fs.writeFileSync(`${DIR}${comparison.diff}`, Flatted.stringify(differences), 'utf8')
  console.log('Done.')

  console.log('\nNew Properties:\n\n', newProperties, '\n')
})
