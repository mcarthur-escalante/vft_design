const fs = require('fs');

const TYPES_TO_ICONS = {
  'Map': 'map.png',
  'Table': 'table.png',
  'SplitPane_horizontal': 'horizontal.png',
  'SplitPane_vertical': 'vertical.png',
  'PlaceHolder': 'placeholder.png',
}

const NON_TILE_TYPES = ['PlaceHolder', 'SplitPane']
const NODE_NUMBER_FIELD = '__NODE_NUMBER__'
const ID_FIELD = '__ID__'
const NAME_FIELD = '__NAME__'
const ICON_FIELD = '__ICON__'
const SUBGRAPHS_FIELD = '__SUBGRAPHS__'
const NODES_FIELD = '__NODES__'
const NODE_DEPENDENCIES_FIELD = '__NODE_DEPENDENCIES__'
const LAYOUT_ID_FIELD = '__LAYOUT_ID__'
const LAYOUT_NAME_FIELD = '__LAYOUT_NAME__'
const BACKGROUND_COLOR_FIELD = '__BACKGROUND_COLOR__'
const TILE_BGCOLOR_FIELD = '__TILE_BGCOLOR__'
const PANE_BGCOLOR_FIELD = '__PANE_BGCOLOR__'
const TILE_ERROR_FIELD = '__TILE_ERROR__'
const TILE_ERROR_MSG_FIELD = '__TILE_ERROR_MSG__'
const GRAPH_TEMPLATE = readFileContents('templates/graph-template.gv')
const SUBGRAPH_TEMPLATE = readFileContents('templates/subgraph-template.gv')
const TILE_TEMPLATE = readFileContents('templates/tile-template.gv')
const PANE_TEMPLATE = readFileContents('templates/pane-template.gv')
const TILE_ERROR_TEMPLATE = readFileContents('templates/tile-error-template.html')

function readFileContents(filename) {
  return fs.readFileSync(filename, 'utf8')
}

function generateLayoutGraphs(filename) {
  const layouts = JSON.parse(readFileContents(filename));

  const graphLayouts = [];
  let nodeNumber = 0;

  layouts.elements.forEach(layout => {
    const hdr = layout.elementHdr;
    const graphLayout = { id: hdr.id, title: hdr.title, active: hdr.active, components: {} };
    const graphComponents = graphLayout.components;
    graphLayouts.push(graphLayout)
    Object.values(layout).forEach(component => {
      if (component === hdr) {
        return;
      }
      graphComponents[component.id] = {
        id: component.id,
        nodeNumber: ++nodeNumber,
        name: null,
        type: component.type,
        icon: null,
        children: [],
      }
    })
    Object.values(layout).forEach(component => {
      if (component === hdr) {
        return;
      }
      const graphComponent = graphComponents[component.id];
      let componentType = NON_TILE_TYPES.includes(component.type) ? component.type : null;
      if (component.props) {
        graphComponent.name = component.props.tileTitle;
        if (!componentType && component.type === 'Tile') {
          componentType = component.props.tileComponentID;
          if (componentType.includes('_')) {
            componentType = componentType.slice(0, componentType.indexOf('_'));
          }
          if (componentType.toUpperCase().includes("TABLE")) {
            componentType = 'Table';
          }
        }
        else if (componentType === 'SplitPane') {
          componentType = component.props.split === 'vertical' ? 'SplitPane_vertical' : 'SplitPane_horizontal'
        }
      }
      graphComponent.icon = TYPES_TO_ICONS[componentType];
      if (!graphComponent.icon) {
        graphComponent.icon = 'undefined.png';
      }
      if (component.parent) {
        const graphComponentParent = graphComponents[component.parent];
        if (graphComponentParent) {
          graphComponentParent.children.push(graphComponent);
        }
        else {
          graphComponent.noParent = component.parent;
        }
      }
    })
  })

  const dotSubgraphs = [];

  graphLayouts.forEach(graphLayout => {
    const dotNodes = [];
    const dotNodeDependencies = [];
    Object.values(graphLayout.components).forEach(graphComponent => {
      graphComponent.children = graphComponent.children
        .sort((gc1, gc2) => parseInt(gc1.id, 10) < parseInt(gc2.id, 10) ? 1 : -1)
        .map((gc, i) => ({ ...gc, ...(i > 1 ? { isInvalidChild: true } : {}) }))
      ;
      graphComponent.children.forEach(child => {
        if (child.isInvalidChild && graphLayout.components[child.id]) {
          graphLayout.components[child.id].isInvalidChild = true;
        }
        dotNodeDependencies.push(
          `    Node_${graphComponent.nodeNumber} -> Node_${child.nodeNumber}` +
          `${child.isInvalidChild
            ? ' [ penwidth = 3, color = "red", fontcolor = "red", label = < <b>Invalid Child</b> > ]' : ''}`
        )
      })
    })
    Object.values(graphLayout.components).forEach(graphComponent => {
      dotNodes.push(
        graphComponent.type === 'SplitPane'
          ? PANE_TEMPLATE
            .replace(NODE_NUMBER_FIELD, graphComponent.nodeNumber)
            .replace(ICON_FIELD, graphComponent.icon)
            .replace(ID_FIELD, graphComponent.id)
            .replace(PANE_BGCOLOR_FIELD, 'gray')
          : TILE_TEMPLATE
            .replace(NODE_NUMBER_FIELD, graphComponent.nodeNumber)
            .replace(ICON_FIELD, graphComponent.icon)
            .replace(ID_FIELD, graphComponent.id)
            .replace(NAME_FIELD, graphComponent.name)
            .replace(TILE_BGCOLOR_FIELD, graphComponent.noParent || graphComponent.isInvalidChild
              ? 'firebrick1'
              : 'darkslategray3'
            )
            .replace(
              TILE_ERROR_FIELD,
              graphComponent.noParent
                ? TILE_ERROR_TEMPLATE
                  .replace(TILE_ERROR_MSG_FIELD, `Parent with ID ${graphComponent.noParent} not found.`)
                : graphComponent.isInvalidChild
                ? TILE_ERROR_TEMPLATE
                  .replace(TILE_ERROR_MSG_FIELD, `Invalid Child.`)
                : ''
            )
            .replace(
              'color=black, ',
              graphComponent.noParent || graphComponent.isInvalidChild
                ? 'color=red, '
                : ''
            )
            .replace(
              'border="1"',
              graphComponent.noParent || graphComponent.isInvalidChild
                ? 'border="3"'
                : 'border="1"'
            )
      )
    })
    dotSubgraphs.push(
      SUBGRAPH_TEMPLATE
        .replace(LAYOUT_ID_FIELD, graphLayout.id)
        .replace(LAYOUT_NAME_FIELD, `< Layout: <b>${graphLayout.title || '[No  Title]'}</b> >`)
        .replace(BACKGROUND_COLOR_FIELD, 'gainsboro' /*graphLayout.active ? 'darkseagreen3' : 'gainsboro'*/)
        .replace(NODES_FIELD, dotNodes.join('\n'))
        .replace(NODE_DEPENDENCIES_FIELD, `\n${dotNodeDependencies.join('\n')}`)
    )
  })

  fs.writeFileSync(filename.replace(".json", ".gv"),
    GRAPH_TEMPLATE
      .replace(SUBGRAPHS_FIELD, dotSubgraphs.join('\n'))
  )
}

if (process.argv.length < 3) {
  console.log('Please provide the JSON filename to generate graph.')
  process.exit(1)
}

generateLayoutGraphs(process.argv[2]);