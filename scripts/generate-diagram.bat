@echo off
set FILENAME=VFT_Client_State_Architecture.puml
set OUT_DIR=..\output
set PLANTUML_JAR=..\lib\plantuml.jar
echo Generating PNG diagram...
java -jar %PLANTUML_JAR% -tpng ..\src\%FILENAME% -o %OUT_DIR%
echo Done.
echo Generating SVG diagram...
java -jar %PLANTUML_JAR% -tsvg ..\src\%FILENAME% -o %OUT_DIR%
echo Done.
echo Generating PDF diagram...
java -jar %PLANTUML_JAR% -tpdf ..\src\%FILENAME% -o %OUT_DIR%
echo Done.
