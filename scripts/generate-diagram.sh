#!/usr/bin/env bash
FILENAME=../src/VFT_Client_State_Architecture.puml
OUT_DIR=../output
PLANTUML_JAR=../lib/plantuml.jar
echo Generating PNG diagram...
java -jar ${PLANTUML_JAR} -tpng ${FILENAME} -o ${OUT_DIR}
echo Done.
echo Generating SVG diagram...
java -jar ${PLANTUML_JAR} -tsvg ${FILENAME} -o ${OUT_DIR}
echo Done.
echo Generating PDF diagram...
java -jar ${PLANTUML_JAR} -tpdf ${FILENAME} -o ${OUT_DIR}
echo Done.
